﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouvement : MonoBehaviour
{
    // Start is called before the first frame update
    /*void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyUp(KeyCode.A))
        {
            //Donothing
        }
        else
        {
            transform.position += transform.forward * Time.deltaTime;
        }
    }*/
    public float minFov = 15F;
    public float maxFov = 90f;
    public float sensitivity = 10f;
 
    
    private void Update() {
        Rigidbody rb = GetComponent<Rigidbody>();
        
        if ((Input.GetKey(KeyCode.A)&& (rb.position.x>200)&& (rb.position.z>0))){
            rb.position+=(Vector3.left);
            rb.position+=(Vector3.back);
        }
        if (Input.GetKey(KeyCode.D)&& (rb.position.x<1040)&& (rb.position.z<850)){
            rb.position+=(Vector3.right);
            rb.position+=(Vector3.forward);
        }
        if (Input.GetKey(KeyCode.W)&& (rb.position.x>200)&& (rb.position.z<850)){
            rb.position+=(Vector3.left);
            rb.position+=(Vector3.forward);
            
           
        }
        if (Input.GetKey(KeyCode.S)&& (rb.position.x<1040)&& (rb.position.z>0)){
            rb.position+=(Vector3.right);
            rb.position+=(Vector3.back);
        }
        float fov = Camera.main.fieldOfView;
        if (fov > 70)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                fov += Input.GetAxis("Mouse ScrollWheel") * sensitivity;
                fov = Mathf.Clamp(fov, minFov, maxFov);
                Camera.main.fieldOfView = fov;
            }
            else
            {
                //Donothing
            }
        }
        else
        {
            fov += Input.GetAxis("Mouse ScrollWheel") * sensitivity;
            fov = Mathf.Clamp(fov, minFov, maxFov);
            Camera.main.fieldOfView = fov; 
        }
        
        
 
    }
    
}
