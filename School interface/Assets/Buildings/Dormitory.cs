﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuildings
{

    public class Dormitory : MonoBehaviour
    {
        public static int PeopleInside;
        private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();
        private Text text;

        void Start()
        {
            text = gameObject.transform.GetChild(0).GetChild(0).Find("Text").GetComponent<Text>();
        }


        void Update()
        {
            PeopleInside = Getbuilding().PeopleInside;
            text.text = "<color=#800000ff><b>" + gameObject.name + "</b></color>" +
                        "\n\n<color=#add8e6ff>People Inside : "
                        + PeopleInside + "</color>\n\n" + "<color=#ff0000ff>Consume : " + Getbuilding().Amount1 +
                        "electricity/day" + "</color>\n\n" + "<color=#008000ff>Refill Sleep</color>";
        }

        public async void ColonEnter(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside++;
            IndoorPop.Add(colon);
            await Task.Delay(20000);
            colon.tiredness = 100;
            ColonOut(colon);
        }

        public void ColonOut(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside--;
            IndoorPop.Remove(colon);
            //get colon out to be coded
        }

        public ColonyStocks.Buidling Getbuilding()
        {
            string SerialNumber = "";
            foreach (var c in gameObject.name)
            {
                if (c >= '0' && c <= '9')
                {
                    SerialNumber += c;
                }
            }

            foreach (var VARIABLE in ColonyStocks.BuildingList)
            {
                if ((VARIABLE.Item1.SerialNumber + 1).ToString() == SerialNumber &&
                    VARIABLE.Item1.Type == ColonyStocks.BuildingType.Dormitory)
                {
                    return VARIABLE.Item1;
                }
            }

            return null;
        }
    }
}