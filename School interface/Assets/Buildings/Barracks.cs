﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuildings
{

    public class Barracks : MonoBehaviour
    {
        public static int PeopleInside;
        private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();

        private float time;
        private bool OnWork;

        private Image img;

        private Text text;

        void Start()
        {
            text = gameObject.transform.GetChild(0).GetChild(0).GetChild(0).Find("Text").GetComponent<Text>();
            time = 30;
            img = gameObject.transform.GetChild(0).GetChild(0).GetChild(0).Find("fill2").GetComponent<Image>();
        }

        async void Update()
        {
            PeopleInside = Getbuilding().PeopleInside;
            text.text = "<color=#800000ff><b>" + gameObject.name + "</b></color>\n\n<color=#add8e6ff>People Inside : " +
                        PeopleInside + "\n\n" + "</color><color=#ff0000ff>Consume : "
                        + Getbuilding().Amount1 + "electricity/day</color>" + "\n\n" +
                        "<color=#008000ff>Train Soldiers</color>";
            if (time < (30 / (PeopleInside + 1)) && OnWork)
            {
                time += Time.deltaTime;
                img.fillAmount = time / (30 / (PeopleInside + 1));
            }

            if (PeopleInside > 0 && !OnWork)
            {
                OnWork = true;
                await Produce();
                OnWork = false;
                ColonOut();
            }
        }

        public void ColonEnter(ColonClass.Human.Soldier colon)
        {
            Getbuilding().PeopleInside++;
            IndoorPop.Add(colon);
            colon.Training = 100;
            colon.Strength++;
        }

        public async Task Produce()
        {
            time = 0;
            while (time < (30 / (PeopleInside + 1)))
            {
                await Task.Delay(25);
            }

            ColonyStocks.Ressource[7] += 2;
            ColonyStocks.Ressource[6] -= 1;
        }

        public void ColonOut()
        {
            Getbuilding().PeopleInside--;
            IndoorPop.RemoveAt(0);
        }

        public ColonyStocks.Buidling Getbuilding()
        {
            string SerialNumber = "";
            foreach (var c in gameObject.name)
            {
                if (c >= '0' && c <= '9')
                {
                    SerialNumber += c;
                }
            }

            foreach (var VARIABLE in ColonyStocks.BuildingList)
            {
                if ((VARIABLE.Item1.SerialNumber + 1).ToString() == SerialNumber &&
                    VARIABLE.Item1.Type == ColonyStocks.BuildingType.Barracks)
                {
                    return VARIABLE.Item1;
                }
            }

            return null;
        }
    }
}
