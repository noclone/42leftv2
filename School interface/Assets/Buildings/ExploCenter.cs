﻿    using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuildings
{

    public class ExploCenter : MonoBehaviour
    {
        public static int ScientInside;
        public static int SoldierInside;
        private List<ColonClass.Human> IndoorScient = new List<ColonClass.Human>();
        private List<ColonClass.Human> IndoorSoldier = new List<ColonClass.Human>();
        private List<ColonClass.Human> Crew = new List<ColonClass.Human>();

        private float time;
        private bool OnWork;

        private Image img;

        private Text text;

        private int lvl;
        private int percentagePeople; // percentage of finding people on explo

        void Start()
        {
            text = gameObject.transform.GetChild(0).GetChild(0).GetChild(0).Find("Text").GetComponent<Text>();
            time = 30;
            img = gameObject.transform.GetChild(0).GetChild(0).GetChild(0).Find("fill2").GetComponent<Image>();
        }

        async void Update()
        {
            lvl = Getbuilding().Level;
            
            ScientInside = Getbuilding().ScientInside;
            SoldierInside = Getbuilding().SoldierInside;

            text.text = "<color=#800000ff><b>" + gameObject.name + "</b></color>" +
                        "\n\n<color=#800000ff>Soldiers Inside : " + SoldierInside +
                        "\n</color><color=#800080ff>Scientists Inside : " + ScientInside + "</color>\n\n" +
                        "<color=#ff0000ff>Consume : " + Getbuilding().Amount1 + "electricity/day</color>" + "\n\n" +
                        "<color=#008000ff>Exploration : </color>"+"\n\n% of finding people : ";

            if (time < (600) && OnWork)
            {
                time += Time.deltaTime;
                img.fillAmount = time / (600);
            }

            if (ScientInside == 1 && SoldierInside == 2 && !OnWork)
            {
                OnWork = true;
                Crew.Add(IndoorScient[0]);
                IndoorScient.RemoveAt(0);
                Crew.Add(IndoorSoldier[0]);
                Crew.Add(IndoorSoldier[1]);
                IndoorSoldier.RemoveAt(1);
                IndoorSoldier.RemoveAt(0);
                await LaunchExplo();
                ScientInside--;
                SoldierInside -= 2;
                for (int i = 2; i >= 0; i--)
                {
                    ColonOut(Crew[i]);
                    Crew.RemoveAt(i);
                }

                OnWork = false;
            }
        }

        public void ColonEnter(ColonClass.Human colon)
        {
            switch (colon.type)
            {
                case ColonClass.ColonType.Scient:
                    ScientInside++;
                    IndoorScient.Add(colon);
                    break;
                case ColonClass.ColonType.Soldier:
                    IndoorSoldier.Add(colon);
                    SoldierInside++;
                    break;
            }
        }

        public void ColonOut(ColonClass.Human colon)
        {

        }

        public async Task LaunchExplo()
        {
            await Task.Delay(600000);
            if (Random.Range(1, 3) == 2)
            {
                ColonyStocks.Ressource[8] += 1*lvl;
            }
        }

        public ColonyStocks.Buidling Getbuilding()
        {
            string SerialNumber = "";
            foreach (var c in gameObject.name)
            {
                if (c >= '0' && c <= '9')
                {
                    SerialNumber += c;
                }
            }

            foreach (var VARIABLE in ColonyStocks.BuildingList)
            {
                if ((VARIABLE.Item1.SerialNumber + 1).ToString() == SerialNumber &&
                    VARIABLE.Item1.Type == ColonyStocks.BuildingType.ExploCenter)
                {
                    return VARIABLE.Item1;
                }
            }

            return null;
        }

    }
}
