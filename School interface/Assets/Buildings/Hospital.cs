﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuildings
{

    public class Hospital : MonoBehaviour
    {
        public static int OccupiedMed;
        public static int AvailableMed;
        public static int PeopleInside;
        private List<ColonClass.Human> WaitingList = new List<ColonClass.Human>();
        private List<ColonClass.Human> MedList = new List<ColonClass.Human>();

        private Text text;

        void Start()
        {
            text = gameObject.transform.GetChild(0).GetChild(0).Find("Text").GetComponent<Text>();
        }

        void Update()
        {

            text.text = "<color=#800000ff><b>" + gameObject.name + "</b></color>"
                        + "\n\n<color=#add8e6ff>Patients : " + Getbuilding().PeopleInside
                        + "\n</color><color=#c0c0c0ff>Occupied Med : " + Getbuilding().OccupiedMed
                        + "\n</color><color=#ffffffff>Available Med : " + Getbuilding().AvailableMed
                        + "\n\n" + "<color=#ff0000ff>Consume : " + Getbuilding().Amount1
                        + "electricity/day</color>" + "\n\n" + "<color=#008000ff>Heal patients in 1 min</color>";

            if (AvailableMed > 0 && WaitingList.Count > 0)
            {
                Getbuilding().AvailableMed--;
                Getbuilding().OccupiedMed++;
                TakeCare(WaitingList[WaitingList.Count - 1]);
                WaitingList.RemoveAt(WaitingList.Count - 1);
            }

        }

        public async void TakeCare(ColonClass.Human colon)
        {
            await Task.Delay(60000);
            colon.health = 100;
            ColonOut(colon);
            Getbuilding().AvailableMed++;
            Getbuilding().OccupiedMed--;
        }

        public void ColonEnter(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside++;
            WaitingList.Add(colon);
        }

        public void ColonOut(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside--;
            //get colon out to be coded
        }

        public void MedEnter(ColonClass.Human Med)
        {
            Getbuilding().AvailableMed++;
            MedList.Add(Med);
        }

        public void MedOut(ColonClass.Human Med)
        {
            Getbuilding().AvailableMed--;
            MedList.Remove(Med);
        }

        public ColonyStocks.Buidling Getbuilding()
        {
            string SerialNumber = "";
            foreach (var c in gameObject.name)
            {
                if (c >= '0' && c <= '9')
                {
                    SerialNumber += c;
                }
            }

            foreach (var VARIABLE in ColonyStocks.BuildingList)
            {
                if ((VARIABLE.Item1.SerialNumber + 1).ToString() == SerialNumber &&
                    VARIABLE.Item1.Type == ColonyStocks.BuildingType.Hospital)
                {
                    return VARIABLE.Item1;
                }
            }

            return null;
        }
    }
}