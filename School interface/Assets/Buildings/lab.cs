﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GameBuildings
{

    public class lab : MonoBehaviour
    {
        public static int PeopleInside;
        private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();

        private float time;
        private bool OnWork;

        private int prob;

        private Image img;

        private Text text;

        private int lvl;

        void Start()
        {
            text = gameObject.transform.GetChild(0).GetChild(0).GetChild(0).Find("Text").GetComponent<Text>();
            time = 300;
            img = gameObject.transform.GetChild(0).GetChild(0).GetChild(0).Find("fill2").GetComponent<Image>();
        }

        async void Update()
        {
            lvl = Getbuilding().Level;
            
            PeopleInside = Getbuilding().PeopleInside;
            text.text = "<color=#800000ff><b>" + gameObject.name + "</b></color>" +
                        "\n\n<color=#add8e6ff>People Inside : "
                        + PeopleInside + "</color>\n\n" + "<color=#ff0000ff>Consume : " + Getbuilding().Amount1 +
                        "electricity/day" + "\n"
                        + Getbuilding().Amount2 + "water/day</color>" + "\n\n" +
                        "<color=#008000ff>Produce skillpoints</color>";
            if (time < (300 - PeopleInside * 15) && OnWork)
            {
                time += Time.deltaTime;
                img.fillAmount = time / (300 - PeopleInside * 15);
            }

            if (PeopleInside > 0 && !OnWork)
            {
                OnWork = true;
                await Produce();
                OnWork = false;
                ColonOut();
            }
        }

        public void ColonEnter(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside++;
            IndoorPop.Add(colon);
        }

        public async Task Produce()
        {
            time = 0;
            while (time < (300 - PeopleInside * 15))
            {
                await Task.Delay(25);
            }

            prob = Random.Range(1, 4);
            if (prob == 2)
            {
                ColonyStocks.Ressource[8] += 1*lvl;
            }
        }

        public void ColonOut()
        {
            Getbuilding().PeopleInside--;
            IndoorPop.RemoveAt(0);
        }

        public ColonyStocks.Buidling Getbuilding()
        {
            string SerialNumber = "";
            foreach (var c in gameObject.name)
            {
                if (c >= '0' && c <= '9')
                {
                    SerialNumber += c;
                }
            }

            foreach (var VARIABLE in ColonyStocks.BuildingList)
            {
                if ((VARIABLE.Item1.SerialNumber + 1).ToString() == SerialNumber &&
                    VARIABLE.Item1.Type == ColonyStocks.BuildingType.Lab)
                {
                    return VARIABLE.Item1;
                }
            }

            return null;
        }
    }
}