﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using Image = UnityEngine.UI.Image;

namespace GameBuildings
{

    public class School : MonoBehaviour
    {
        private ColonManager x;

        private int WaitingChildren = 15;

        private int FarmerStudents;
        private int EngiStudents;
        private int MedStudents;
        private int ScientStudents;
        private int SoldierStudents;
        private int WorkerStudents;

        private Text text;
        private Font Arial;
        private GameObject Students;
        private Text StudentsNb;
        private GameObject content;
        private Text myText;

        private Image fill1;
        private Image fill2;
        private Image fill3;
        private Image fill4;
        private Image fill5;
        private Image fill6;

        private float time1 = 100000;
        private float time2 = 100000;
        private float time3 = 100000;
        private float time4 = 100000;
        private float time5 = 100000;
        private float time6 = 100000;

        void Start()
        {
            x = GameObject.Find("ColonManager").GetComponent<ColonManager>();
            text = GameObject.Find("Waiting Children").GetComponent<Text>();
            Arial = Resources.GetBuiltinResource<Font>("Arial.ttf");
            StudentsNb = GameObject.Find("ListTitle").GetComponent<Text>();

            Students = new GameObject("Students");
            content = GameObject.Find("Content");
            Students.transform.SetParent(content.transform, false);
            myText = Students.AddComponent<Text>();
            myText.font = Arial;
            myText.color = Color.black;
            myText.transform.position = StudentsNb.transform.position + Vector3.down * 55 + Vector3.right * 40;

            fill1 = gameObject.transform.GetChild(0).GetChild(0).Find("Scroll View").GetChild(0).GetChild(0)
                .Find("fill")
                .GetComponent<Image>();
            fill2 = gameObject.transform.GetChild(0).GetChild(0).Find("Scroll View").GetChild(0).GetChild(0)
                .Find("fill2")
                .GetComponent<Image>();
            fill3 = gameObject.transform.GetChild(0).GetChild(0).Find("Scroll View").GetChild(0).GetChild(0)
                .Find("fill3")
                .GetComponent<Image>();
            fill4 = gameObject.transform.GetChild(0).GetChild(0).Find("Scroll View").GetChild(0).GetChild(0)
                .Find("fill4")
                .GetComponent<Image>();
            fill5 = gameObject.transform.GetChild(0).GetChild(0).Find("Scroll View").GetChild(0).GetChild(0)
                .Find("fill5")
                .GetComponent<Image>();
            fill6 = gameObject.transform.GetChild(0).GetChild(0).Find("Scroll View").GetChild(0).GetChild(0)
                .Find("fill6")
                .GetComponent<Image>();
        }

        void Update()
        {

            UpdateText();

            StudentsNb.text = "<color=#add8e6ff>Students : " +
                              (WorkerStudents + FarmerStudents + EngiStudents + MedStudents +
                               ScientStudents + SoldierStudents + "</color>");

            if (time1 < 3)
            {
                time1 += Time.deltaTime;
                fill1.fillAmount = time1 / 3;
            }

            if (time2 < 3)
            {
                time2 += Time.deltaTime;
                fill2.fillAmount = time2 / 3;
            }

            if (time3 < 3)
            {
                time3 += Time.deltaTime;
                fill3.fillAmount = time3 / 3;
            }

            if (time4 < 3)
            {
                time4 += Time.deltaTime;
                fill4.fillAmount = time4 / 3;
            }

            if (time5 < 3)
            {
                time5 += Time.deltaTime;
                fill5.fillAmount = time5 / 3;
            }

            if (time6 < 3)
            {
                time6 += Time.deltaTime;
                fill6.fillAmount = time6 / 3;
            }

            text.text = ("<color=#add8e6ff>Waiting Children : " + WaitingChildren + "</color>");
        }

        private void UpdateText()
        {
            myText.text = FarmerStudents + "\n"
                                         + EngiStudents + "\n"
                                         + MedStudents + "\n"
                                         + ScientStudents + "\n"
                                         + SoldierStudents + "\n"
                                         + WorkerStudents;
        }

        //To run when a child enters the school
        public void ChildEnter()
        {
            WaitingChildren++;
        }

        public async void SetFarmer()
        {
            if (WaitingChildren > 0)
            {
                WaitingChildren--;
                FarmerStudents++;
                int wait1 = ((FarmerStudents - 1) * 3000) - 1000 * (int) time1;
                if (wait1 < 0)
                    wait1 = 0;
                await Task.Delay(wait1);
                time1 = 0;
                CreateColon(ColonClass.ColonType.Farmer);
            }
        }

        public async void SetEngi()
        {
            if (WaitingChildren > 0)
            {
                WaitingChildren--;
                EngiStudents++;
                int wait2 = ((EngiStudents - 1) * 3000) - 1000 * (int) time2;
                if (wait2 < 0)
                    wait2 = 0;
                await Task.Delay(wait2);
                time2 = 0;
                CreateColon(ColonClass.ColonType.Engi);
            }
        }

        public async void SetMed()
        {
            if (WaitingChildren > 0)
            {
                WaitingChildren--;
                MedStudents++;
                int wait3 = ((MedStudents - 1) * 3000) - 1000 * (int) time3;
                if (wait3 < 0)
                    wait3 = 0;
                await Task.Delay(wait3);
                time3 = 0;
                CreateColon(ColonClass.ColonType.Med);
            }
        }

        public async void SetScient()
        {
            if (WaitingChildren > 0)
            {
                WaitingChildren--;
                ScientStudents++;
                int wait4 = ((ScientStudents - 1) * 3000) - 1000 * (int) time4;
                if (wait4 < 0)
                    wait4 = 0;
                await Task.Delay(wait4);
                time4 = 0;
                CreateColon(ColonClass.ColonType.Scient);
            }
        }

        public async void SetSoldier()
        {
            if (WaitingChildren > 0)
            {
                WaitingChildren--;
                SoldierStudents++;
                int wait5 = ((SoldierStudents - 1) * 3000) - 1000 * (int) time5;
                if (wait5 < 0)
                    wait5 = 0;
                await Task.Delay(wait5);
                time5 = 0;
                CreateColon(ColonClass.ColonType.Soldier);
            }
        }

        public async void SetWorker()
        {
            if (WaitingChildren > 0)
            {
                WaitingChildren--;
                WorkerStudents++;
                int wait6 = ((WorkerStudents - 1) * 3000) - 1000 * (int) time6;
                if (wait6 < 0)
                    wait6 = 0;
                await Task.Delay(wait6);
                time6 = 0;
                CreateColon(ColonClass.ColonType.Worker);
            }
        }

        private async void CreateColon(ColonClass.ColonType var)
        {
            await Task.Delay(3000); //900000
            switch (var)
            {
                case ColonClass.ColonType.Farmer:
                    x.CreateFarmer();
                    FarmerStudents--;
                    break;
                case ColonClass.ColonType.Engi:
                    x.CreateEngi();
                    EngiStudents--;
                    break;
                case ColonClass.ColonType.Med:
                    x.CreateMed();
                    MedStudents--;
                    break;
                case ColonClass.ColonType.Scient:
                    x.CreateScient();
                    ScientStudents--;
                    break;
                case ColonClass.ColonType.Soldier:
                    x.CreateSoldier();
                    SoldierStudents--;
                    break;
                case ColonClass.ColonType.Worker:
                    x.CreateWorker();
                    WorkerStudents--;
                    break;
            }

        }


    }
}
