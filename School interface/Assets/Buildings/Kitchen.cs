﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Schema;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameBuildings
{

    public class Kitchen : MonoBehaviour
    {
        public static int PeopleInside;
        private List<ColonClass.Human> IndoorPop = new List<ColonClass.Human>();

        private Text text;

        void Start()
        {
            text = gameObject.transform.GetChild(0).GetChild(0).Find("Text").GetComponent<Text>();
        }


        void Update()
        {
            PeopleInside = Getbuilding().PeopleInside;
            text.text = "<color=#800000ff><b>" + gameObject.name + "</b></color>" +
                        "\n\n<color=#add8e6ff>People Inside : " + PeopleInside + "</color>\n\n"
                        + "<color=#ff0000ff>Consume : " + Getbuilding().Amount1 + "electricity/day</color>" + "\n\n" +
                        "<color=#008000ff>Refill Food and Thirst</color>";
        }

        public async void ColonEnter(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside++;
            IndoorPop.Add(colon);
            await Task.Delay(10000);
            colon.Food = 100;
            colon.Thirst = 100;
            ColonyStocks.Ressource[2] -= 10;
            ColonyStocks.Ressource[1] -= 1;
            ColonOut(colon);
        }

        public void ColonOut(ColonClass.Human colon)
        {
            Getbuilding().PeopleInside--;
            IndoorPop.Remove(colon);
        }

        public ColonyStocks.Buidling Getbuilding()
        {
            string SerialNumber = "";
            foreach (var c in gameObject.name)
            {
                if (c >= '0' && c <= '9')
                {
                    SerialNumber += c;
                }
            }

            foreach (var VARIABLE in ColonyStocks.BuildingList)
            {
                if ((VARIABLE.Item1.SerialNumber + 1).ToString() == SerialNumber &&
                    VARIABLE.Item1.Type == ColonyStocks.BuildingType.Kitchen)
                {
                    return VARIABLE.Item1;
                }
            }

            return null;
        }

    }

}