﻿using System.Collections;
using System.Collections.Generic;
using Polybrush;
using UnityEngine;

namespace GameBuildings
{

    public class structure : MonoBehaviour
    {
        public Material matOk, matFail, matDefault;
        public bool okay = false;
        public Renderer rend;
        private int objCount;

        void Start()
        {
            rend = GetComponent<Renderer>();
        }

        void Update()
        {
            bool i = 1 != placmentscript.numberofthebuilding;
            if (objCount > 1 && i)
            {
                rend.material = matFail;
                okay = false;
            }
            else if (objCount <= 1 && i)
            {
                rend.material = matOk;
                okay = true;
            }
            //Hitbox maison buguer, donc 4 points de collision
            else if (objCount > 4)
            {
                rend.material = matFail;
                okay = false;
            }
            else if (objCount <= 4)
            {
                rend.material = matOk;
                okay = true;
            }
        }

//Lorsque ça touche ou ne touche plus, objCount gagner +ou- 1 collision
        void OnCollisionEnter(Collision col)
        {
            objCount++;
        }

        void OnCollisionExit(Collision col)
        {
            objCount--;
        }
    }
}